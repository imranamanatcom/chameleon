### Jalsonic Chameloen 1.0 Beta plugin for jQuery - Beauty of every screen ### 
A Division of Jalsonic Networks, Inc..


### What Does Chameloen Do, Exactly? ###
Good question! I'm so glad you asked.

Chameloen allows you to customize your CSS for most commonly screen resolution used pages for desktops and tablets in landscape mode. we build responsive websites using CHAMELEOn that look an any screen size from smartphones  and tablets to laptops and large display.


### Getting Started ###
First load the seed files, if you haven't yet.

*<script src="jquery-1.11.3.js" type="text/javascript"></script>
<script src="chameloen-1.0.js" type="text/javascript"></script>*

### Chameleon generated HTML tag ###

<HTML class="MacOS MacSnowLeopard safari safari5 largeDesktop view960"
responsive-framework="chameleon-0.1"
data-useragent="mozilla/5.0 (macintosh; u; intel ma...ko) version/5.0.4 safari/533.20.27"
chameleon-status="200-OK">






Designed and built with all the love in the world by Imran Malik and Jalsonic Secure Team. Maintained by the Jalsonic Networks.